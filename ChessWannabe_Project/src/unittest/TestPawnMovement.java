package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.Pawn;

public class TestPawnMovement extends Test {

	public TestPawnMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test Pawn Movement");
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new Pawn(g.getPlayer1()));
		
		
		g.getBoard().getGameBoard()[4][6].setPieceInPlace(new Pawn(g.getPlayer1()));
		g.getBoard().getGameBoard()[5][7].setPieceInPlace(new Pawn(g.getPlayer1()));
		g.getBoard().getGameBoard()[5][5].setPieceInPlace(new Pawn(g.getPlayer2()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[1][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 2);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[6][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 1);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 1);
		
		// TEST PAWN ATTACK
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][6].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 2);
		
		
		g.getBoard().getGameBoard()[4][6].setPieceInPlace(null);
		g.getBoard().getGameBoard()[5][7].setPieceInPlace(null);
		g.getBoard().getGameBoard()[5][5].setPieceInPlace(null);
		
		System.out.println("PAWN MOVEMENT - TEST PASSED");

	}

}
