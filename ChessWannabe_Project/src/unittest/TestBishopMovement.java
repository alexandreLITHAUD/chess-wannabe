package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.Bishop;

public class TestBishopMovement extends Test {

	public TestBishopMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test Bishop Movement");
		
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new Bishop(g.getPlayer1()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[0][2].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[7][2].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 6);
		
		System.out.println("BISHOP MOVEMENT - TEST PASSED");

	}

}
