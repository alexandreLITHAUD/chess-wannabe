package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.King;
import modele.pieces.Pawn;

public class TestKingMovement extends Test{

	
	public TestKingMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test King Movement");
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new King(g.getPlayer1()));
		
		
		g.getBoard().getGameBoard()[4][6].setPieceInPlace(new King(g.getPlayer1()));
		g.getBoard().getGameBoard()[5][6].setPieceInPlace(new King(g.getPlayer1()));
		g.getBoard().getGameBoard()[5][5].setPieceInPlace(new King(g.getPlayer2()));
		g.getBoard().getGameBoard()[4][5].setPieceInPlace(new King(g.getPlayer2()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[0][4].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[7][4].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 8);
		
		// TEST KING ATTACK
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][6].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 7);
		
		
		g.getBoard().getGameBoard()[4][6].setPieceInPlace(null);
		g.getBoard().getGameBoard()[5][6].setPieceInPlace(null);
		g.getBoard().getGameBoard()[5][5].setPieceInPlace(null);
		g.getBoard().getGameBoard()[4][5].setPieceInPlace(null);
		
		System.out.println("KING MOVEMENT - TEST PASSED");

	}
	
}
