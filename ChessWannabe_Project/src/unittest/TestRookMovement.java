package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.Rook;

public class TestRookMovement extends Test{

	public TestRookMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test Rook Movement");
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new Rook(g.getPlayer1()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[0][0].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[7][0].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 11);
		
		System.out.println("ROOK MOVEMENT - TEST PASSED");

	}
	
	
}
