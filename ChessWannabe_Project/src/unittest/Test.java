package unittest;

public abstract class Test {
	
	
	protected void computeTest(boolean cond) {
		if(!cond) {
			System.err.println("Test Failed");
			System.exit(1);
			return;
		}
		System.out.println("Section Passed");
		return;
	}

}
