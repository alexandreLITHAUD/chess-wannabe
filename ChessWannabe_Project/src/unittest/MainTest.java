package unittest;

import javax.swing.JFrame;

import modele.game.Game;

public class MainTest extends Test {
	
	public static void main(String[] args) {
		
		Game g = new Game(new JFrame());
		
		//System.out.println(g.getGameBoard().toString());
		
		System.out.println("==============================================");
		System.out.println("Test Movement Pieces");
		System.out.println("==============================================");
		
		Test tpm = new TestPawnMovement(g);
		
		Test tbm = new TestBishopMovement(g);
		
		Test trm = new TestRookMovement(g);
		
		Test tkm = new TestKnightMovement(g);
		
		Test tqm = new TestQueenMovement(g);
		
		Test kingMovement = new TestKingMovement(g);
		
		
		System.out.println("\n");
		System.out.println("==============================================");
		System.out.println("Test Movement PASSED");
		System.out.println("==============================================");
		
	}

}
