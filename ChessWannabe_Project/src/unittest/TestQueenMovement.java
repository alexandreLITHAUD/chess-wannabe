package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.Queen;

public class TestQueenMovement extends Test{
	
	
	public TestQueenMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test Queen Movement");
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new Queen(g.getPlayer1()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[0][3].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[7][3].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 0);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 16);
		
		System.out.println("QUEEN MOVEMENT - TEST PASSED");

	}
}
