package unittest;

import java.util.ArrayList;

import modele.game.Game;
import modele.move.Move;
import modele.pieces.Bishop;
import modele.pieces.Knight;

public class TestKnightMovement extends Test{
	
	public TestKnightMovement(Game g) {

		System.out.println("\n");
		System.out.println("Test Knight Movement");
		
		
		g.getBoard().getGameBoard()[4][1].setPieceInPlace(new Knight(g.getPlayer1()));
		
		ArrayList<Move> legalMoves = new ArrayList<>();
		
		// TEST BLOCKED PIECE PLAYER 1 
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[0][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 2);
		
		// TEST BLOCKED PIECE PLAYER 2
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[7][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 2);
		
		// TEST FREE PIECE IN 4 - 1
		legalMoves = (ArrayList<Move>) g.getBoard().getGameBoard()[4][1].getPieceInPlace().getLegalMoves(g.getBoard());
		this.computeTest(legalMoves.size() == 6);
		
		System.out.println("KNIGHT MOVEMENT - TEST PASSED");

	}

}
