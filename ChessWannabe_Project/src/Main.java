import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import modele.game.Game;

public class Main {
	
	static JFrame frame;
	
	
	public static void main(String[] args) {
		System.out.println("Hello World");
		
		createWindow();
		
		Game g = new Game(frame);
	}
	
	private static void createWindow() {
		Main.frame = new JFrame("Chess Wannabe");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//JLabel textLabel = new JLabel("I'm a label in the window",SwingConstants.CENTER);
		//textLabel.setPreferredSize(new Dimension(300, 100)); 
		
		//frame.getContentPane().add(textLabel);
		
		//frame.pack();
		
		frame.setVisible(true);
		
	}

}
