package modele.board;

import modele.pieces.Piece;

/**
 * Class that will reprensent a Case of the Game Board
 * @author alexandre
 */
public class Case {
	
	
	private Piece pieceInPlace;
	private int xCoordinate;
	private int yCoordinate;
	
	
	/**
	 * Constructor of the Case class
	 */
	public Case(int x, int y, Piece p) {
		this.xCoordinate = x;
		this.yCoordinate = y;
		
		this.pieceInPlace = p;
	}


	public Piece getPieceInPlace() {
		return pieceInPlace;
	}

	public void initPieceInPlace(Piece pieceInPlace) {
		this.pieceInPlace = pieceInPlace;
		
		if(pieceInPlace != null) {
			this.pieceInPlace.setxCoordinate(xCoordinate);
			this.pieceInPlace.setyCoordinate(yCoordinate);
			
			this.pieceInPlace.getPlayer().addPiece(pieceInPlace);
		}
	}

	public void setPieceInPlace(Piece pieceInPlace) {
		this.pieceInPlace = pieceInPlace;
		
		if(pieceInPlace != null) {
			this.pieceInPlace.setxCoordinate(xCoordinate);
			this.pieceInPlace.setyCoordinate(yCoordinate);	
		}
	}


	public int getxCoordinate() {
		return xCoordinate;
	}


	public int getyCoordinate() {
		return yCoordinate;
	}


	@Override
	public String toString() {
		
		if(this.pieceInPlace == null) {
			return "  ";
		}
		else {
			return this.pieceInPlace.toString();
		}
		
	}	
	

}
