package modele.board;

import java.util.Arrays;

import modele.game.Game;
import modele.pieces.Bishop;
import modele.pieces.King;
import modele.pieces.Knight;
import modele.pieces.Pawn;
import modele.pieces.Queen;
import modele.pieces.Rook;

/**
 * Class that will reprensent the board of the Game
 * 
 * @author alexandre
 */
public class Board {

	private Case[][] gameBoard;
	private Game game;

	/**
	 * Constructor of the Board Generate all the Cases and piece at the right place
	 */
	public Board(Game g) {

		this.game = g;

		this.gameBoard = new Case[8][8];

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				this.gameBoard[i][j] = new Case(i, j, null);
			}
		}

		initGamePieces();

	}

	private void initGamePieces() {

		//
		for (int j = 0; j < 8; j++) {
			this.gameBoard[1][j].initPieceInPlace(new Pawn(this.game.getPlayer1())); // PLAYER 1
		}

		for (int j = 0; j < 8; j++) {
			this.gameBoard[6][j].initPieceInPlace(new Pawn(this.game.getPlayer2())); // PLAYER 2
		}

		// HARDCODE KEK
		// PLAYER 1
		this.gameBoard[0][0].initPieceInPlace(new Rook(this.game.getPlayer1()));
		this.gameBoard[0][1].initPieceInPlace(new Knight(this.game.getPlayer1()));
		this.gameBoard[0][2].initPieceInPlace(new Bishop(this.game.getPlayer1()));
		this.gameBoard[0][3].initPieceInPlace(new Queen(this.game.getPlayer1()));
		this.gameBoard[0][4].initPieceInPlace(new King(this.game.getPlayer1()));
		this.gameBoard[0][5].initPieceInPlace(new Bishop(this.game.getPlayer1()));
		this.gameBoard[0][6].initPieceInPlace(new Knight(this.game.getPlayer1()));
		this.gameBoard[0][7].initPieceInPlace(new Rook(this.game.getPlayer1()));

		// HARDCODE KEK
		// PLAYER 2
		this.gameBoard[7][0].initPieceInPlace(new Rook(this.game.getPlayer2()));
		this.gameBoard[7][1].initPieceInPlace(new Knight(this.game.getPlayer2()));
		this.gameBoard[7][2].initPieceInPlace(new Bishop(this.game.getPlayer2()));
		this.gameBoard[7][3].initPieceInPlace(new Queen(this.game.getPlayer2()));
		this.gameBoard[7][4].initPieceInPlace(new King(this.game.getPlayer2()));
		this.gameBoard[7][5].initPieceInPlace(new Bishop(this.game.getPlayer2()));
		this.gameBoard[7][6].initPieceInPlace(new Knight(this.game.getPlayer2()));
		this.gameBoard[7][7].initPieceInPlace(new Rook(this.game.getPlayer2()));
	}

	public Case[][] getGameBoard() {
		return gameBoard;
	}

	@Override
	public String toString() {

		String str = "";

		for(int i = 0;i<8;i++) {
			str += "    " + i;
		}
		
		str += ("\n");
		
		for (int i = 0; i < 8; i++) {
			str += (i + " | ");
			for (int j = 0; j < 8; j++) {
				str += (this.gameBoard[i][j].toString() + " | ");
			}
			str += ("\n");
		}
		return str;
	}

}
