package modele.move;

import modele.board.Board;
import modele.board.Case;
import modele.pieces.Pawn;
import modele.pieces.Piece;

public class MovePiece extends Move {

	public MovePiece(Case destination, Board gameBoard, Case start) {
		super(destination, gameBoard, start);
		// TODO Auto-generated constructor stub
	}
	
	public void execute() {
		
		Piece p = this.start.getPieceInPlace();
		
		if(p instanceof Pawn) { ((Pawn) p).setCanDoubleJump(false); }
		
		this.start.setPieceInPlace(null);
		this.destination.setPieceInPlace(p);
		
	}
	
}
