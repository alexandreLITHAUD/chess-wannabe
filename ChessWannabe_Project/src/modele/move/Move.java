package modele.move;

import modele.board.Board;
import modele.board.Case;

public abstract class Move {
	
	protected Case destination;
	protected Board gameBoard;
	protected Case start;
	
	public Case getDestination() {
		return destination;
	}

	public Case getStart() {
		return start;
	}

	public Move(Case destination, Board gameBoard, Case start) {
		super();
		this.destination = destination;
		this.gameBoard = gameBoard;
		this.start = start;
	}
	
	public abstract void execute();
}

