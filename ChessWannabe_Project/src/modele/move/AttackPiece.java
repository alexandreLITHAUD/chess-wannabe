package modele.move;

import modele.board.Board;
import modele.board.Case;
import modele.pieces.Piece;

public class AttackPiece extends Move {

	public AttackPiece(Case destination, Board gameBoard, Case start) {
		super(destination, gameBoard, start);
		// TODO Auto-generated constructor stub
	}
	
	public void execute() {
		
		Piece p = this.start.getPieceInPlace();
		
		this.start.setPieceInPlace(null);
		
		Piece dead = this.destination.getPieceInPlace();
		p.getPlayer().addScore(dead.getPieceValue());
		//this.destination.setPieceInPlace(null);
		
		this.destination.setPieceInPlace(p);
	}
	
}
