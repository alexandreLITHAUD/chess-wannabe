package modele.utils;

import modele.board.Case;
import modele.game.Player;
import modele.pieces.Piece;

public class BoardUtils {

	
	public static boolean isOutOfBound(Case c) {
		
		if(c.getxCoordinate() < 0 || c.getxCoordinate() > 7 || c.getyCoordinate() < 0 || c.getyCoordinate() > 7) {
			return true;
		}
		return false;
	}
	
	public static boolean isOutOfBound(int x, int y) {
		
		if(x < 0 || x > 7 || y < 0 || y > 7) {
			return true;
		}
		return false;
	}
	
	
	public static boolean isCaseAttackable(Piece p, Case c) {
		
		if(c.getPieceInPlace() == null) {
			return false;
		}
		if(c.getPieceInPlace().getPlayer() == p.getPlayer()) {
			return false;
		}
		
		return true;
		
	}
	
	public static boolean isAlly(Piece p, Case c) {
		
		if(c.getPieceInPlace() == null) {
			return false;
		}
		
		if(c.getPieceInPlace().getPlayer() == p.getPlayer()) {
			return true;
		}
		
		return false;
	}
	
}
