package modele.game;

public class Player2 extends Player{
	
	@Override
	public String toString() {
		return "2";
	}

	@Override
	public TypePlayer getTypePlayer() {
		return TypePlayer.Player2;
	}
}
