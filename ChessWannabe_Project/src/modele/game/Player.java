package modele.game;

import java.util.ArrayList;
import java.util.Collection;

import modele.pieces.King;
import modele.pieces.Piece;

public abstract class Player {

	protected int score;
	
	protected ArrayList<Piece> pieces = new ArrayList<>();
	protected King kingPlayer;
	
	protected boolean isChecked = false;
	protected boolean isThreathend = false;
	
	public void addPiece(Piece p) {
		if(p instanceof King) {
			this.kingPlayer = (King)p;
		}else {
			this.pieces.add(p);
		}
	}
	
	public Collection<Piece> getPieces(){
		return (Collection<Piece>)this.pieces;
	}
	
	public King getKing() {
		return this.kingPlayer;
	}
	
	public int getScore() {
		return score;
	}

	public void addScore(int score) {
		this.score += score;
	}

	public abstract TypePlayer getTypePlayer();
	
}
