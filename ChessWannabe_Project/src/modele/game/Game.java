package modele.game;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import modele.board.Board;
import modele.board.Case;
import modele.move.Move;

/**
 * Class representing the Game as a whole
 * @author alexandre
 */
public class Game {
	
	private JFrame frame;
	
	private Board gameBoard;
	private Player player1;
	private Player player2;
	
	private Player playerInPlay;
	
	private boolean isGameOver = false;
	
	private Map<Case,JButton> mapCaseButton = new HashMap<>();
	
	private Map<Case,Move> mapMoveTarget = new HashMap<>();
	
	/**
	 * Constructor of the game
	 */
	public Game(JFrame frame) {
		
		this.frame = frame;
		this.player1 = new Player1();
		this.player2 = new Player2();
		this.gameBoard = new Board(this);
		
		this.playerInPlay = this.player1;
		
		System.out.println(this.gameBoard.toString());
		
		startGame();
		
	}
	
	
	public void startGame() {
		
		initVisual();
		
		while(isGameOver) {}
		
	}

	private void initVisual() {
		
		frame.setLayout(new GridLayout(8,8));
		
		for(int i = 0; i < 8;i++) {
			for(int j = 0; j < 8; j++) {
				
				JButton b;
				Case c = this.gameBoard.getGameBoard()[i][j];
				
				if(c.getPieceInPlace() != null){
					b = new JButton(new ImageIcon(c.getPieceInPlace().getImagePath()));
				}else {
					b = new JButton(new ImageIcon("src/ressource/blank.png"));
				}
				
				this.mapCaseButton.put(c, b);
				
				b.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						updateVisual();
						
						Case c = computeCase(b.getX(), b.getY(), b.getHeight(), b.getWidth());
						
						
						// IF TARGETTED MOVE !
						if(mapMoveTarget.containsKey(c)) {
							
							mapMoveTarget.get(c).execute();
							
							if(playerInPlay == player1) {
								playerInPlay = player2;
							}else {
								playerInPlay = player1;
							}
							
							mapMoveTarget.clear();
							
							updateVisual();
							return;
							
						}else {
							mapMoveTarget.clear();
						}
						
						if(c.getPieceInPlace() != null) {
							if(c.getPieceInPlace().getPlayer() != playerInPlay) {
								return;
							}
						}
						
						
						if(c.getPieceInPlace() == null) { return; }
						for(Move m : c.getPieceInPlace().getLegalMoves(gameBoard)) {
							
							Case dest = m.getDestination();
							JButton buttonDest = mapCaseButton.get(dest);
							buttonDest.setIcon(new ImageIcon("src/ressource/record.png"));
							
							mapMoveTarget.put(dest, m);
						}
					}

				});
				
				this.frame.getContentPane().add(b);
			}
		}
		
		
		frame.setSize(800,800);
		
	}
	
	private void updateVisual() {
		
		for(int i = 0; i < 8;i++) {
			for(int j = 0; j < 8; j++) {
				
				Case c = this.gameBoard.getGameBoard()[i][j];
				JButton jb = this.mapCaseButton.get(c);
				
				if(c.getPieceInPlace() != null){
					jb.setIcon(new ImageIcon(c.getPieceInPlace().getImagePath()));
				}else {
					jb.setIcon(new ImageIcon("src/ressource/blank.png"));
				}
				
			}
		}
	}


	public Board getBoard() {
		return gameBoard;
	}

	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}
	
	public Case computeCase(int x, int y, int h, int w) {
		return this.gameBoard.getGameBoard()[Math.round(y/h)][Math.round(x/w)];
	}

}
