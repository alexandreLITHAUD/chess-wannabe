package modele.pieces;

import java.util.ArrayList;
import java.util.Collection;

import modele.board.Board;
import modele.board.Case;
import modele.game.Player;
import modele.game.TypePlayer;
import modele.move.AttackPiece;
import modele.move.Move;
import modele.move.MovePiece;
import modele.utils.BoardUtils;

public class King extends Piece {

	public King(Player p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "#" + this.player.toString();
	}

	@Override
	public Collection<Move> getLegalMoves(Board gameBoard) {

		ArrayList<Move> legalMoves = new ArrayList<>();
		Case dest;
		Case sta = gameBoard.getGameBoard()[xCoordinate][yCoordinate];

		// N
		if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// S
		if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// E
		if (!BoardUtils.isOutOfBound(xCoordinate, yCoordinate + 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate][yCoordinate + 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// W
		if (!BoardUtils.isOutOfBound(xCoordinate, yCoordinate - 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate][yCoordinate - 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// NE
		if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate + 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate + 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// NW
		if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate - 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate - 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// SE
		if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate + 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate + 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		// SW
		if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate - 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate - 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		return legalMoves;

	}

	@Override
	public int getPieceValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getImagePath() {
		// TODO Auto-generated method stub
		if(this.player.getTypePlayer() == TypePlayer.Player1) {
			return "src/ressource/king1.png";
		}
		return "src/ressource/king2.png";
	}
}
