package modele.pieces;

import java.util.ArrayList;
import java.util.Collection;

import modele.board.Board;
import modele.board.Case;
import modele.game.Player;
import modele.game.TypePlayer;
import modele.move.AttackPiece;
import modele.move.Move;
import modele.move.MovePiece;
import modele.utils.BoardUtils;

public class Knight extends Piece{
	
	public Knight(Player p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "K" + this.player.toString();
	}

	@Override
	public Collection<Move> getLegalMoves(Board gameBoard) {
		ArrayList<Move> legalMoves = new ArrayList<>();
		Case dest;
		Case sta = gameBoard.getGameBoard()[xCoordinate][yCoordinate];

		/**
		 * 		XX	
		 * 		X
		 * 		P
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate - 2, yCoordinate + 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 2][yCoordinate + 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	   XX	
		 * 		X
		 * 		P
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate - 2 , yCoordinate - 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 2][yCoordinate - 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    P	
		 * 		X
		 * 		XX
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate + 2, yCoordinate + 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 2][yCoordinate + 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    P	
		 * 		X
		 * 	   XX
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate + 2, yCoordinate - 1)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 2][yCoordinate - 1];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    	X
		 * 		P X X
		 * 	    
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate + 2)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate + 2];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    	
		 * 		P X X
		 * 	    	X
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate + 2)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate + 2];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    X	
		 * 		X X P
		 * 	    	
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate - 2)) {
			dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate - 2];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		/**
		 * 	    	
		 * 		X X P
		 * 	    X	
		 */
		if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate - 2)) {
			dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate - 2];
			if (dest.getPieceInPlace() == null) {
				// MOVE MOVE
				legalMoves.add(new MovePiece(dest, gameBoard, sta));
			} else if(BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
			}
		}

		return legalMoves;
	}

	@Override
	public int getPieceValue() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public String getImagePath() {
		
		if(this.player.getTypePlayer() == TypePlayer.Player1) {
			return "src/ressource/chess1.png";
		}
		return "src/ressource/chess2.png";
	}
}
