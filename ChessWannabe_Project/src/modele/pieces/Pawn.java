package modele.pieces;

import java.util.ArrayList;
import java.util.Collection;

import modele.board.Board;
import modele.board.Case;
import modele.game.Player;
import modele.game.TypePlayer;
import modele.move.AttackPiece;
import modele.move.Move;
import modele.move.MovePiece;
import modele.utils.BoardUtils;

public class Pawn extends Piece {

	public boolean canDoubleJump;

	public Pawn(Player p) {
		super(p);
		this.canDoubleJump = true;
	}

	@Override
	public String toString() {
		return "P" + this.player.toString();
	}

	@Override
	public Collection<Move> getLegalMoves(Board gameBoard) {

		ArrayList<Move> legalMoves = new ArrayList<>();

		Case sta = gameBoard.getGameBoard()[xCoordinate][yCoordinate];

		Case dest;

		if (this.player.getTypePlayer() == TypePlayer.Player2) {

			// DOUBLE JUMP
			if (canDoubleJump) {

				// Si la coordonnée est libre et il n'y a pas de piece
				if (!BoardUtils.isOutOfBound(xCoordinate - 2, yCoordinate)) {
					dest = gameBoard.getGameBoard()[xCoordinate - 2][yCoordinate];
					if (dest.getPieceInPlace() == null) {
						// MOVE MOVE
						legalMoves.add(new MovePiece(dest, gameBoard, sta));
					}
				}
			}

			// AVANCE 1 SINON
			if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate)) {
				dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate];
				if (dest.getPieceInPlace() == null) {
					// MOVE MOVE
					legalMoves.add(new MovePiece(dest, gameBoard, sta));
				}
			}

			// AttackMove Droite
			if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate + 1)) {
				dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate + 1];
				if (BoardUtils.isCaseAttackable(this, dest)) {
					// ATTACK MOVE
					legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				}
			}

			// AttackMove Gauche
			if (!BoardUtils.isOutOfBound(xCoordinate - 1, yCoordinate - 1)) {
				dest = gameBoard.getGameBoard()[xCoordinate - 1][yCoordinate - 1];
				if (BoardUtils.isCaseAttackable(this, dest)) {
					// ATTACK MOVE
					legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				}
			}

		} else {

			// DOUBLE JUMP
			if (canDoubleJump) {

				// Si la coordonnée est libre et il n'y a pas de piece
				if (!BoardUtils.isOutOfBound(xCoordinate + 2, yCoordinate)) {
					dest = gameBoard.getGameBoard()[xCoordinate + 2][yCoordinate];
					if (dest.getPieceInPlace() == null) {
						// MOVE MOVE
						legalMoves.add(new MovePiece(dest, gameBoard, sta));
					}
				}
			}

			// AVANCE 1 SINON
			if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate)) {
				dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate];
				if (dest.getPieceInPlace() == null) {
					// MOVE MOVE
					legalMoves.add(new MovePiece(dest, gameBoard, sta));
				}
			}

			// AttackMove Droite
			if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate + 1)) {
				dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate + 1];
				if (BoardUtils.isCaseAttackable(this, dest)) {
					// ATTACK MOVE
					legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				}
			}

			// AttackMove Gauche
			if (!BoardUtils.isOutOfBound(xCoordinate + 1, yCoordinate - 1)) {
				dest = gameBoard.getGameBoard()[xCoordinate + 1][yCoordinate - 1];
				if (BoardUtils.isCaseAttackable(this, dest)) {
					// ATTACK MOVE
					legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				}
			}
		}
		
		return legalMoves;
	}

	public boolean isCanDoubleJump() {
		return canDoubleJump;
	}

	public void setCanDoubleJump(boolean canDoubleJump) {
		this.canDoubleJump = canDoubleJump;
	}

	@Override
	public int getPieceValue() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getImagePath() {
		if(this.player.getTypePlayer() == TypePlayer.Player1) {
			return "src/ressource/pawn1.png";
		}
		return "src/ressource/pawn2.png";
	}

}
