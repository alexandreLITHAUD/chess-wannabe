package modele.pieces;

import java.util.ArrayList;
import java.util.Collection;

import modele.board.Board;
import modele.board.Case;
import modele.game.Player;
import modele.game.TypePlayer;
import modele.move.AttackPiece;
import modele.move.Move;
import modele.move.MovePiece;
import modele.utils.BoardUtils;

public class Queen extends Piece{
	
	
	public Queen(Player p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Q" + this.player.toString();
	}

	@Override
	public Collection<Move> getLegalMoves(Board gameBoard) {
		ArrayList<Move> legalMoves = new ArrayList<>();
		Case dest;
		Case sta = gameBoard.getGameBoard()[xCoordinate][yCoordinate];
		int i = 1;

		// DIAGONALE H G TANT QUE NON PIECE OU OOB
		while (!BoardUtils.isOutOfBound(xCoordinate - i, yCoordinate - i)) {
			dest = gameBoard.getGameBoard()[xCoordinate - i][yCoordinate - i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// DIAGONALE H D TANT QUE NON PIECE OU OOB
		while (!BoardUtils.isOutOfBound(xCoordinate - i, yCoordinate + i)) {
			dest = gameBoard.getGameBoard()[xCoordinate - i][yCoordinate + i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// DIAGONALE B G TANT QUE NON PIECE OU OOB
		while (!BoardUtils.isOutOfBound(xCoordinate + i, yCoordinate - i)) {
			dest = gameBoard.getGameBoard()[xCoordinate + i][yCoordinate - i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// DIAGONALE B D TANT QUE NON PIECE OU OOB
		while (!BoardUtils.isOutOfBound(xCoordinate + i, yCoordinate + i)) {
			dest = gameBoard.getGameBoard()[xCoordinate + i][yCoordinate + i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}
		
		// H
		while (!BoardUtils.isOutOfBound(xCoordinate - i, yCoordinate)) {
			dest = gameBoard.getGameBoard()[xCoordinate - i][yCoordinate];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// B
		while (!BoardUtils.isOutOfBound(xCoordinate + i, yCoordinate)) {
			dest = gameBoard.getGameBoard()[xCoordinate + i][yCoordinate];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// G
		while (!BoardUtils.isOutOfBound(xCoordinate, yCoordinate - i)) {
			dest = gameBoard.getGameBoard()[xCoordinate][yCoordinate - i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		i = 1;

		// D
		while (!BoardUtils.isOutOfBound(xCoordinate, yCoordinate + i)) {
			dest = gameBoard.getGameBoard()[xCoordinate][yCoordinate + i];

			if(BoardUtils.isAlly(this, dest)) {
				break;
			}
			
			if (BoardUtils.isCaseAttackable(this, dest)) {
				legalMoves.add(new AttackPiece(dest, gameBoard, sta));
				break;
			}

			legalMoves.add(new MovePiece(dest, gameBoard, sta));

			i++;
		}

		return legalMoves;
	}

	@Override
	public int getPieceValue() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public String getImagePath() {
		if(this.player.getTypePlayer() == TypePlayer.Player1) {
			return "src/ressource/queen1.png";
		}
		return "src/ressource/queen2.png";
	}
}
