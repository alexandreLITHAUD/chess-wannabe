package modele.pieces;

import java.util.Collection;

import modele.board.Board;
import modele.game.Player;
import modele.move.Move;

public abstract class Piece {

	protected Player player;
	protected int xCoordinate;
	protected int yCoordinate;
	
	public Piece(Player p) {
		this.player = p;
	}
	
	public abstract Collection<Move> getLegalMoves(Board gameBoard);
	
	public abstract int getPieceValue();
	
	public abstract String getImagePath();

	public int getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(int xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public int getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(int yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public Player getPlayer() {
		return player;
	}
	
}
