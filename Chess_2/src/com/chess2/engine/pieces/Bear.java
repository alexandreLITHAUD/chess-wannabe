package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.moves.Move;
import com.chess2.engine.players.Alliance;

public class Bear extends Piece{

	public Bear(int piecePosition, PieceType pieceType) {
		super(piecePosition, pieceType, Alliance.NULL);
	}

	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return PieceType.BEAR.toString();
	}

}
