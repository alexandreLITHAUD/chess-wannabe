package com.chess2.engine.pieces;

public enum PieceType {
	
	FISH("P", 100),
	ELEPHANT("E", 300),
	ROOK("R", 500),
	MONKEY("M", 700),
	QUEEN("Q", 2000),
	KING("K", 1000),
	BEAR("BEAR", 200),
	FISH_QUEEN("PQ", 1500);
	
	private String pieceName;
	private int pieceValue;
	
	PieceType(String pieceName, int pieceValue) {
		this.pieceName = pieceName;
		this.pieceValue = pieceValue;
	}
	
	@Override
	public String toString() {
		return this.pieceName;
	}
	
	public int getPieceValue() {
		return this.pieceValue;
	}
	

}
