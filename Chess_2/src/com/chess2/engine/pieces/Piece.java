package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.moves.Move;
import com.chess2.engine.players.Alliance;

public abstract class Piece {
	
	private int piecePosition;
	private PieceType pieceType;
	private Alliance pieceAlliance;
	
	public Piece(int piecePosition, PieceType pieceType, Alliance pieceAlliance) {
		super();
		this.piecePosition = piecePosition;
		this.pieceType = pieceType;
		this.pieceAlliance = pieceAlliance;
	}
	
	public int getPiecePosition() {
		return piecePosition;
	}

	public PieceType getPieceType() {
		return pieceType;
	}

	public Alliance getPieceAlliance() {
		return pieceAlliance;
	}

	public abstract Collection<Move> getLegalMoves(Board board);

}
