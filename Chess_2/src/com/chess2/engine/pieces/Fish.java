package com.chess2.engine.pieces;

import java.util.Collection;

import com.chess2.engine.board.Board;
import com.chess2.engine.moves.Move;
import com.chess2.engine.players.Alliance;

public class Fish extends Piece{

	public Fish(int piecePosition, PieceType pieceType, Alliance pieceAlliance) {
		super(piecePosition, pieceType, pieceAlliance);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Collection<Move> getLegalMoves(Board board) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return PieceType.FISH.toString();
	}

}
