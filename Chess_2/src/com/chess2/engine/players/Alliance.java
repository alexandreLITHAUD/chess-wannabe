package com.chess2.engine.players;

import com.chess2.engine.board.BoardUtils;

public enum Alliance {
	WHITE
 {	
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public boolean isFishPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return BoardUtils.EIGHTH_RANK[position];
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return -1;
		}
 },
	BLACK {
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return true;
		}
		
		@Override
		public boolean isFishPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return BoardUtils.FIRST_RANK[position];
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return 1;
		}
	},
	
	NULL {
		@Override
		public boolean isWhite() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isBlack() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public int getDirection() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean isFishPromotionSquare(int position) {
			// TODO Auto-generated method stub
			return false;
		}
	};
	
	private boolean canRookKill = false;
	
	public abstract boolean isWhite();
	public abstract boolean isBlack();
	public abstract int getDirection();
	
	public boolean canRookKill() { // ONLY IF ALLIANCE GOT A PIECE KILLED IN LAST TURN SO THE PLAYER WILL HAVE TO UPDATE THAT !
		return this.canRookKill;
	}
		
	public void setCanRookKill(boolean b) {
		this.canRookKill = b;
	}
	
	public abstract boolean isFishPromotionSquare(int position);
	
}
