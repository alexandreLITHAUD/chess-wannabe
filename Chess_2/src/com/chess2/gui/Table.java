package com.chess2.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Table {

	private static Dimension FRAME_DIMENSION = new Dimension(800,800);
	
	private JFrame gameWindow;
	
	private static Table INSTANCE = new Table();
	
	
	private Table() {
		
		this.gameWindow = new JFrame("Chess 2 by Oats Jenkins");
		this.gameWindow.setLayout(new BorderLayout());
		
		this.gameWindow.setSize(FRAME_DIMENSION);
		
		this.gameWindow.setVisible(true);
		
	}
	
	public static Table get() {
		return INSTANCE;
	}
	
	public void show() {
		// CLEAR ALL THAT HAS BEEN CREATED !!
	}
	
	
}
