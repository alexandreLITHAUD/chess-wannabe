package com.chess.engine.board;

import com.chess.engine.board.Board.Builder;
import com.chess.engine.pieces.Pawn;
import com.chess.engine.pieces.Piece;
import com.chess.engine.pieces.Rook;

public abstract class Move {

	protected final Board board;
	protected final Piece movedPiece;
	protected final int destinationCoordinate;
	protected final boolean isFirstMove;

	public static final Move NULL_MOVE = new NullMove();

	private Move(final Board board, final Piece movedPiece, final int destinationCoordinate) {
		this.board = board;
		this.movedPiece = movedPiece;
		this.destinationCoordinate = destinationCoordinate;
		this.isFirstMove = movedPiece.isFirstMove();
	}

	private Move(final Board board, final int destinationCoordinate) {
		this.board = board;
		this.destinationCoordinate = destinationCoordinate;
		this.movedPiece = null;
		this.isFirstMove = false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		result = prime * result + destinationCoordinate;
		result = prime * result + (isFirstMove ? 1231 : 1237);
		result = prime * result + ((movedPiece == null) ? 0 : movedPiece.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Move))
			return false;
		final Move otherMove = (Move) obj;

//		return getDestinationCoordinate() == otherMove.getDestinationCoordinate()
//				&& this.getMovedPiece().equals(otherMove.getMovedPiece());

		return this.getCurrentCoordinate() == otherMove.getCurrentCoordinate()
				&& getDestinationCoordinate() == otherMove.getDestinationCoordinate()
				&& this.getMovedPiece().equals(otherMove.getMovedPiece());
	}

	public int getDestinationCoordinate() {
		return this.destinationCoordinate;
	}

	public int getCurrentCoordinate() {
		return this.getMovedPiece().getPiecePosition();
	}

	public Piece getMovedPiece() {
		return this.movedPiece;
	}

	public Board getBoard() {
		return this.board;
	}

	public boolean isAttack() {
		return false;
	}

	public boolean isCastlingMove() {
		return false;
	}

	public Piece getAttackedPiece() {
		return null;
	}

	public Board execute() {

		final Builder builder = new Builder();

		for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
			if (!this.movedPiece.equals(piece)) {
				builder.setPiece(piece);
			}
		}

		for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
			builder.setPiece(piece);
		}

		builder.setPiece(this.movedPiece.movePiece(this));
		builder.setMoveMaker(this.board.currentPlayer().getOpponent().getAlliance());

		return builder.build();
	}

	// INNER CONCRETE CLASS
	public static final class MajorMove extends Move {

		public MajorMove(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof MajorMove && super.equals(obj);
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardUtils.getPositionAtCoordinate(this.destinationCoordinate);
		}

	}

	public static final class MajorAttackMove extends AttackMove {

		public MajorAttackMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Piece attackedPiece) {
			super(board, movedPiece, destinationCoordinate, attackedPiece);
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof MajorAttackMove && super.equals(obj);
		}

		@Override
		public String toString() {
			return movedPiece.getPieceType().toString()
					+ BoardUtils.getPositionAtCoordinate(this.destinationCoordinate);
		}

	}

	// INNER CONCRETE CLASS
	public static class AttackMove extends Move {

		private final Piece attackedPiece;

		public AttackMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Piece attackedPiece) {
			super(board, movedPiece, destinationCoordinate);
			this.attackedPiece = attackedPiece;
		}

		@Override
		public boolean isAttack() {
			return true;
		}

		@Override
		public Piece getAttackedPiece() {
			return this.attackedPiece;
		}

		@Override
		public int hashCode() {
			return this.attackedPiece.hashCode() + super.hashCode();
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof AttackMove))
				return false;
			final AttackMove otherAttckMove = (AttackMove) obj;
			return super.equals(otherAttckMove) && this.getAttackedPiece().equals(otherAttckMove.getAttackedPiece());
		}

	}

	public static final class PawnPromotion extends Move {

		final Move decoratedMove;
		final Pawn promotedPawn;
		
		final Piece promotionPiece;

		public PawnPromotion(final Move decoratedMove) {
			super(decoratedMove.getBoard(), decoratedMove.getMovedPiece(), decoratedMove.getDestinationCoordinate());

			this.decoratedMove = decoratedMove;
			this.promotedPawn = (Pawn) decoratedMove.getMovedPiece();
			this.promotionPiece = this.promotedPawn.getPromotionPiece();
		}

		@Override
		public int hashCode() {
			return decoratedMove.hashCode() + (31 * promotedPawn.hashCode()) + (31 * promotionPiece.hashCode());
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof PawnPromotion && super.equals(obj);
		}

		@Override
		public Board execute() {
			
			final Board pawnMovedBoard = this.decoratedMove.execute();
			
			final Builder builder = new Builder();
			for(final Piece piece : pawnMovedBoard.currentPlayer().getActivePieces()) {
				if(!this.promotedPawn.equals(piece)) {
					builder.setPiece(piece);
				}
			}
			
			for(final Piece piece : pawnMovedBoard.currentPlayer().getOpponent().getActivePieces()) {
				builder.setPiece(piece);
			}
			
			builder.setPiece(this.promotionPiece.movePiece(this));
			builder.setMoveMaker(pawnMovedBoard.currentPlayer().getAlliance()); // ? 43 - 22:55
			return builder.build();
		}

		@Override
		public boolean isAttack() {
			return this.decoratedMove.isAttack();
		}

		@Override
		public Piece getAttackedPiece() {
			return this.decoratedMove.getAttackedPiece();
		}

		@Override
		public String toString() {
			return decoratedMove.toString() + "=" + promotionPiece.getPieceType().toString();
		}

	}

	// INNER CONCRETE CLASS
	public static final class PawnMove extends Move {

		public PawnMove(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof PawnMove && super.equals(obj);
		}

		@Override
		public String toString() {
			return BoardUtils.getPositionAtCoordinate(this.destinationCoordinate);
		}

	}

	// INNER CONCRETE CLASS
	public static class PawnAttackMove extends AttackMove {

		public PawnAttackMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Piece attackedPiece) {
			super(board, movedPiece, destinationCoordinate, attackedPiece);
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof PawnAttackMove && super.equals(obj);
		}

		@Override
		public String toString() {
			return BoardUtils.getPositionAtCoordinate(this.movedPiece.getPiecePosition()).substring(0, 1) + "x"
					+ BoardUtils.getPositionAtCoordinate(this.destinationCoordinate);
		}

	}

	// INNER CONCRETE CLASS
	public static final class PawnEnPassantAttackMove extends PawnAttackMove {

		public PawnEnPassantAttackMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Piece attackedPiece) {
			super(board, movedPiece, destinationCoordinate, attackedPiece);
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof PawnEnPassantAttackMove && super.equals(obj);
		}

		@Override
		public Board execute() {
			final Builder builder = new Builder();

			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {

				if (!this.movedPiece.equals(piece)) {
					builder.setPiece(piece);
				}

			}

			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				if (!piece.equals(this.getAttackedPiece())) {
					builder.setPiece(piece);
				}
			}

			builder.setPiece(this.movedPiece.movePiece(this));
			builder.setMoveMaker(this.board.currentPlayer().getOpponent().getAlliance());
			return builder.build();
		}

	}

	// INNER CONCRETE CLASS
	public static final class PawnJump extends Move {

		public PawnJump(final Board board, final Piece movedPiece, final int destinationCoordinate) {
			super(board, movedPiece, destinationCoordinate);
		}

		@Override
		public Board execute() {

			final Builder builder = new Builder();

			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
				if (!this.movedPiece.equals(piece)) {
					builder.setPiece(piece);
				}
			}

			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				builder.setPiece(piece);
			}

			final Pawn movedPawn = (Pawn) this.movedPiece.movePiece(this);

			builder.setPiece(movedPawn);
			builder.setEnPassantPawn(movedPawn);
			builder.setMoveMaker(this.board.currentPlayer().getOpponent().getAlliance());
			return builder.build();
		}

		@Override
		public String toString() {
			return BoardUtils.getPositionAtCoordinate(this.destinationCoordinate);
		}

	}

	// INNER ABSTRACT CLASS
	static abstract class CastleMove extends Move {

		protected final Rook castleRook;
		protected final int castleRookStart;
		protected final int castleRookDestination;

		public CastleMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Rook castleRook, final int castleRookStart, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate);

			this.castleRook = castleRook;
			this.castleRookStart = castleRookStart;
			this.castleRookDestination = castleRookDestination;
		}

		public Rook getCastleRook() {
			return this.castleRook;
		}

		@Override
		public boolean isCastlingMove() {
			return true;
		}

		@Override
		public Board execute() {

			final Builder builder = new Builder();
			for (final Piece piece : this.board.currentPlayer().getActivePieces()) {
				if (!this.movedPiece.equals(piece) && !this.castleRook.equals(piece)) {
					builder.setPiece(piece);
				}
			}

			for (final Piece piece : this.board.currentPlayer().getOpponent().getActivePieces()) {
				builder.setPiece(piece);
			}

			builder.setPiece(this.movedPiece.movePiece(this));
			builder.setPiece(new Rook(this.castleRook.getPieceAlliance(), this.castleRookDestination));
			builder.setMoveMaker(this.board.currentPlayer().getOpponent().getAlliance());
			return builder.build();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((castleRook == null) ? 0 : castleRook.hashCode());
			result = prime * result + castleRookDestination;
			result = prime * result + castleRookStart; // ?
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!(obj instanceof CastleMove))
				return false;
			final CastleMove otherCastleMove = (CastleMove) obj;
			return super.equals(otherCastleMove) && this.castleRook.equals(otherCastleMove.getCastleRook());
		}

	}

	// INNER CONCRETE CLASS
	public static final class KingSideCastleMove extends CastleMove {

		public KingSideCastleMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Rook castleRook, final int castleRookStart, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate, castleRook, castleRookStart, castleRookDestination);
		}

		@Override
		public String toString() {
			return "O-O";
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof KingSideCastleMove && super.equals(obj);
		}

	}

	// INNER CONCRETE CLASS
	public static final class QueenSideCastleMove extends CastleMove {

		public QueenSideCastleMove(final Board board, final Piece movedPiece, final int destinationCoordinate,
				final Rook castleRook, final int castleRookStart, final int castleRookDestination) {
			super(board, movedPiece, destinationCoordinate, castleRook, castleRookStart, castleRookDestination);
		}

		@Override
		public String toString() {
			return "O-O-O";
		}

		@Override
		public boolean equals(final Object obj) {
			return this == obj || obj instanceof QueenSideCastleMove && super.equals(obj);
		}

	}

	// INNER CONCRETE CLASS
	public static final class NullMove extends Move {

		public NullMove() {
			super(null, 65);
		}

		@Override
		public Board execute() {
			throw new RuntimeException("Cannot execute an Invalid Move !");
		}

		@Override
		public int getCurrentCoordinate() {
			return -1;
		}

	}

	public static class MoveFactory {

		private MoveFactory() {
			throw new RuntimeException("Cannot instanciate this class !");
		}

		public static Move createMove(final Board board, final int currentCoordinate, final int destinationCoordinate) {

			for (final Move move : board.getAllLegalMoves()) {
				if (move.getCurrentCoordinate() == currentCoordinate
						&& move.getDestinationCoordinate() == destinationCoordinate) {
					return move;
				}
			}

			return NULL_MOVE;
		}

	}

}
