package com.chess.engine.board;

import java.util.HashMap;
import java.util.Map;

import com.chess.engine.pieces.Piece;
import com.google.common.collect.ImmutableMap;

//IMMUTABLE CLASS
public abstract class Tile {

	protected final int tileCoordinate;

	// MAP that will serve as a Static Factory that will
	// search load an Object if it has already been created
	private static final Map<Integer, EmptyTile> EMPTY_TILES_CACHE = createAllPossibleEmptyTiles();

	private static Map<Integer, EmptyTile> createAllPossibleEmptyTiles() {

		final Map<Integer, EmptyTile> emptyTileMap = new HashMap<>();

		for (int i = 0; i < BoardUtils.NUM_TILES; i++) {
			emptyTileMap.put(i, new EmptyTile(i));
		}

		return ImmutableMap.copyOf(emptyTileMap);
	}

	public static Tile createTile(final int tileCoordinate, final Piece piece) {
		return piece != null ? new OccupiedTile(tileCoordinate, piece) : EMPTY_TILES_CACHE.get(tileCoordinate);
	}

	private Tile(final int tileCorrdinate) {
		this.tileCoordinate = tileCorrdinate;
	}
	
	public int getTileCoordinate() {
		return this.tileCoordinate;
	}

	public abstract boolean isTileOccupied();

	public abstract Piece getPiece();

	// IMMUTABLE SUB CLASS
	public static final class EmptyTile extends Tile {

		private EmptyTile(final int tileCorrdinate) {
			super(tileCorrdinate);
		}

		@Override
		public boolean isTileOccupied() {
			return false;
		}

		@Override
		public Piece getPiece() {
			return null;
		}

		@Override
		public String toString() {
			return "-";
		}

	}

	// IMMUTABLE SUB CLASS
	public static final class OccupiedTile extends Tile {

		private final Piece pieceOnTile;

		private OccupiedTile(final int tileCorrdinate, final Piece pieceOnTile) {
			super(tileCorrdinate);
			this.pieceOnTile = pieceOnTile;
		}

		@Override
		public boolean isTileOccupied() {
			return true;
		}

		@Override
		public Piece getPiece() {
			return this.pieceOnTile;
		}

		@Override
		public String toString() {
			return this.pieceOnTile.getPieceAlliance().isBlack() ? this.pieceOnTile.toString().toLowerCase()
					: this.pieceOnTile.toString();
		}

	}

}
