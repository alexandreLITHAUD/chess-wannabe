package com.chess.engine.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.chess.engine.Alliance;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.MoveStatus;
import com.chess.engine.board.MoveTransition;
import com.chess.engine.pieces.King;
import com.chess.engine.pieces.Piece;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public abstract class Player {

	protected final Board board;
	protected final King playerKing; // A LA BASE C'EST KING MAIS POUE LE CONSTRUCTEUR DE KINGCASTLE NE VEUX PAS
	protected final Collection<Move> legalMoves;

	private boolean isInCheck;

	public Player(final Board board, final Collection<Move> legalMoves, final Collection<Move> oppenentMoves) {
		this.board = board;
		this.playerKing = establishKing();
		this.legalMoves = ImmutableList
				.copyOf(Iterables.concat(legalMoves, calculateKingCastles(legalMoves, oppenentMoves)));
		this.isInCheck = !Player.calculateAttacksOnTile(this.playerKing.getPiecePosition(), oppenentMoves).isEmpty();
	}

	protected static Collection<Move> calculateAttacksOnTile(int piecePosition, Collection<Move> moves) {

		final List<Move> attackMoves = new ArrayList<>();

		for (final Move move : moves) {
			if (piecePosition == move.getDestinationCoordinate()) {
				attackMoves.add(move);
			}
		}
		return ImmutableList.copyOf(attackMoves);

	}

	public King getPlayerKing() {
		return this.playerKing;
	}

	public Collection<Move> getLegalMoves() {
		return this.legalMoves;
	}

	private King establishKing() {

		for (final Piece piece : getActivePieces()) {
			if (piece.getPieceType().isKing()) {
				return (King) piece;
			}
		}
		throw new RuntimeException("Should not happend : Player without King piece !");
	}

	public boolean isMoveLegal(final Move move) {
		return this.legalMoves.contains(move);
	}

	public boolean isInCheck() {
		return this.isInCheck;
	}

	public boolean isInCheckMate() {
		return this.isInCheck && !hasEscapemove();
	}

	public boolean isInStaleMate() {
		return !this.isInCheck && !hasEscapemove();
	}

	protected boolean hasEscapemove() {

		for (final Move move : this.legalMoves) {
			final MoveTransition transition = makeMove(move);
			if (transition.getMoveStatus().isDone()) {
				return true;
			}
		}
		return false;
	}

	public boolean isCastled() {
		return false;
	}

	public MoveTransition makeMove(final Move move) {

		if (!isMoveLegal(move)) {
			return new MoveTransition(board, move, MoveStatus.ILLEGAL_MOVE);
		}

		final Board transitionBoard = move.execute();

		final Collection<Move> kingAttacks = Player.calculateAttacksOnTile(
				transitionBoard.currentPlayer().getOpponent().getPlayerKing().getPiecePosition(),
				transitionBoard.currentPlayer().getLegalMoves());

		if (!kingAttacks.isEmpty()) {
			return new MoveTransition(this.board, move, MoveStatus.LEAVES_PLAYER_IN_CHECK);
		}

		return new MoveTransition(transitionBoard, move, MoveStatus.DONE);
	}

	public abstract Collection<Piece> getActivePieces();

	public abstract Alliance getAlliance();

	public abstract Player getOpponent();

	protected abstract Collection<Move> calculateKingCastles(final Collection<Move> playerLegals,
			final Collection<Move> opponentLegals);

}
