# Chess Wannabe

> author : @alexandreLITHAUD

This project is about doing a basic chess game in Java.



**Just a little project**

- [Chess Wannabe](#chess-wannabe)
- [Project Information](#project-information)
  - [Name](#name)
  - [Description](#description)
  - [Installation](#installation)
  - [Roadmap](#roadmap)
  - [Special Thanks](#special-thanks)
  - [Project status](#project-status)
- [My Version](#my-version)
  - [Unit Test](#unit-test)
  - [Modele](#modele)
  - [View](#view)
  - [Controler](#controler)
- [Tutorial Verion](#tutorial-verion)

---

# Project Information

## Name
This project is name that way because it probably wont work in the end.

It is just a way for me to oppupied myself and to have fun.

## Description
This project will be two diffent chess game project : one made by myself in about a week the other will be the whole game that a will recreate via a tutorial on Youtube.

I will try to impove this version at the end by adding couple a thing I will think about during the developpement.

## Installation
- A .jar will be available in later version of this project
- You will have to use the command ` java -jar [project_name].jar ` in order to launch it.

## Roadmap
- **7 days** : my version of a chess engine from scratch (try my best)
- **after that** : Tutorial version
- **at the end** : addon to the last version and maybe making my one fonctional if not already

## Special Thanks
- Thank you to **Software Architecture & Design** for this tutorial that helped me quite a lot improving my skills : [***link***](https://www.youtube.com/watch?v=h8fSdSUKttk&list=PLOJzCFLZdG4zk5d-1_ah2B4kqZSeIlWtt)

## Project status
 - ***This project is for educational purposes only.***


---

# My Version

## Unit Test
- Different unit tests are present for the main action of the game
- All the test are visible [**here**](./ChessWannabe_Project/src/unittest/)

## Modele
- The modele of the game is [**here**](./ChessWannabe_Project/src/modele/)

## View 
- The View part is NYI

## Controler
- The Controler part is NYI

---

# Tutorial Verion

